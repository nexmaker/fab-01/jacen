
# 1.1 markdown 练习
[nexpcb的网站，链接的引用](http://www.nexpcb.com)
![图片的引用](https://cdn2.hubspot.net/hubfs/3027780/coronabanner.png)
[markdown 认识与入](https://sspai.com/post/25137)

表头1|表头2|表头3
----|:----|:----
张三|李四|王五
---
分割线使用---或者***

<font face="黑体" color=green size=10>我是黑体，绿色尺寸为10</font>
***

<font face="宋体" color=red size=5>我是黑宋体，红色尺寸为5</font>
***
多行代码使用“```”  一行代码使用“`”

`define`

```
#define
#ifndifne
main()
```

*斜体一个“*” *
**加粗2个“*”**
***加粗斜体三个星号***
~~删除线是波浪线2个~~

# 一级标题一个“#”
## 二级标题
### 三级标题
### 四级标题
## 5级标题
### 6级标题
##### 最多只有6级标题

>这是引用的内容使用>好
>>这也是引用的内容
>>>再次一级引用的内容
>>>>>>>>8个斜杆
>>>>>>>>>>9个了

---
# 1.2 git相关

## 1.2.1 VScode
* vs code 安装后预览效果需要安装2个插件，markdown all in one 和markdown preview enhanced；
* 需要配置名字和邮箱，`git config --global user.name ""`  和 `git config --global user.email " "`给所有的用户都配置上，否则会提示配置问题
![](https://gitlab.com/picbed/bed/uploads/221505f6b95205f15f63d0cd0bd47a68/config.png)
* 软件打开直接克隆到本地，和进入文件夹`git init`功能一样
* 如果文件有修改，保存之后会有提示，电机“+”号暂存修改，点击“√”提交，提交信息，回车，“。。。”--推送，完成，如下图示![](https://gitlab.com/0505/fablab/uploads/da173877376cf969880ede23096ea025/20200330162059.png)
![](https://gitlab.com/0505/fablab/uploads/923a0f1948dd41ac1c3091c7ea64abe4/20200330162215.png)
![](https://gitlab.com/0505/fablab/uploads/5d330518ebd8c57f5854a92fffe86989/20200330162450.png)
![](https://gitlab.com/0505/fablab/uploads/283309919bbd3ba78185f34a9acff9de/20200330162325.png)
* 上述的步骤原因是，以bala的图示说明
![](https://gitlab.com/0505/fablab/uploads/ba265f0a98713f221d9bf2cd50628ab6/20200330162921.png)
* 在重新安装了candence后出现断链问题!是因为密钥被删除了，在生成密钥的时候会自动将密钥放置到candence下面，需要输入/D等其他盘来修改路径，重新设定密钥，备份本地文件夹，之后设定密钥，克隆到本地，将新文件覆盖掉本地，上传即可[](https://gitlab.com/0505/fablab/uploads/4d87064f10a9556e446f0f243bb2f6dd/20200410155602.png)

## 1.2.2 gitbook安装
* node安装正常，不过如果通过npm安装gitbook提示已经安装的错误则，将现在的所有安装目录都删除，然后`npm install cnpm -g`先安装cmpm 基于国内网站进行安装![](https://gitlab.com/0505/fablab/uploads/af05f7b60277902fb5a504b1378881dd/clipboard.png)  
* 然后安装`cmpm i gitbook-cli -g`![](https://gitlab.com/0505/fablab/uploads/6c8d66e7176ae614f26673968a80438c/clipboard1.png)
* `gitbook intall`![](https://gitlab.com/0505/fablab/uploads/532a7900c2b9c106fb77c53705029b7d/clipboard3.png)
* `gitbook fetch pre`版本切换![](https://gitlab.com/0505/fablab/uploads/07c3206b77a65b16c26f1eb35e5bdf63/clipboard4.png)


## 1.2.3 gitbook学习
* gitbook是基于node.js的命令行工具,需要下载安装node.js
* 在你想创建的文件夹下面鼠标右键即可在当前文件夹下进入git bash;vscode 的终端还是有问题的，需要分析解决![](https://gitlab.com/0505/fablab/uploads/500f517d8357a5b723004ede5d01e8f5/20200330165323.png)
* 可以在这里初始化你的书本![](https://gitlab.com/0505/fablab/uploads/b1b19aae75f8d97a52bd5a1f470d52dd/20200330165459.png)
* 初始化会自动加入一些内容，主要是新增summary作为书目，你原来编辑的内容会被覆盖![](https://gitlab.com/0505/fablab/uploads/4bc94e62b4da36ef982f407897405551/20200330165746.png)


## 1.2.4 picgo
* [一个中文的使用说明](https://picgo.github.io/PicGo-Doc/zh/guide/#%E5%BA%94%E7%94%A8%E8%AF%B4%E6%98%8E)
* picgo的组合项目名很重要,这个是我自己创建的组和项目名才能上传。![](https://gitlab.com/0505/fablab/uploads/0fe5b894bf92dc17fe09461bf3a04a41/20200330154431.png)
* 使用bob的组则不能上传![](https://gitlab.com/0505/fablab/uploads/5a4058b29ce38c35aac80d865048fa02/20200330154619.png)

* 在获取token的时候这个日期不要填写![](https://gitlab.com/0505/fablab/uploads/a76067be966f47ed218038bd3ef49dac/20200330154931.png)
* 截图上传比较方便，拷贝连接粘贴到vscode会直接完成语句
* 删除本地相册不影响已经上传的图片
