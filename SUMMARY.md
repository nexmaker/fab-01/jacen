# My notes and homeworks

## Notes

* [Solidworks.notes](doc\soldworks\notes.md)
* [Markdown.grammer](doc\notes\markdown.md)


## Homeworks
* [1.Lession.Project management](doc\project\gitprojectmanagement.md)

* [2.Lession.fusion360](doc\CAD\fusion360.md)

* [3.Lession.print3d](doc\3Dprint\print3D.md)

* [4.Lession.layoutbycandence](doc\EDA Layout\cendence.md)

* [5.Lession.ardunio]()
