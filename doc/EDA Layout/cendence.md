# 笔记
  
* 在原理图检查的时候有个特殊错误![](https://gitlab.com/0505/fablab/uploads/6ed76686db6a61134d4d5275d0c5df63/20200408193751.png)
* 可以看到在这个文件的117行实际上是空格![](https://gitlab.com/0505/fablab/uploads/5317e1c8b0521872a8a69552880aefb9/20200408193737.png)
* 最后发现是因为封装中的![](https://gitlab.com/0505/fablab/uploads/2a599e723c8403fa2ff325921dcce03f/20200408193839.png)
![](https://gitlab.com/0505/fablab/uploads/b6efaed820f58724ca5c1c7058982239/20200408183424.png)
* 封装中不能有空格，点等特殊符号 ![](https://gitlab.com/0505/fablab/uploads/96f09697b660f488d7e09768006fcd69/20200408192559.png)![](https://gitlab.com/0505/fablab/uploads/b8fb0cb3f9ad31c038f2fb47b1008ba1/20200408191358.png)
* 在话原理图之后再修改封装提示出错，需要更新cache![](https://gitlab.com/0505/fablab/uploads/342d8942e42567b038165ae974334f92/20200408193951.png)
* 路径设置比较重要![](https://gitlab.com/0505/fablab/uploads/a4dcd637459ec2813e49d901e4452265/20200408194846.png)